describe('ViralDrumSequencer', function() {
	beforeEach(function() {
		var ctx = new AudioContext();
		this.vds = new ViralDrumSequencer(ctx);
		expect(this.vds).toBeDefined();
	});


	it("should be possible to create a SoundBank",function() {
		this.sb = new this.vds.model.SoundBank("testbank");
		expect(this.sb).toBeDefined();
	});
	it("should be possible to load a soundBuffer and create a SoundObject",function() {
		var so = new this.vds.model.SoundObject("testlyd");
		this.sb.register(so);

		var xhr = new XMLHttpRequest();
		xhr.open('GET', '../test/assets/SoundCollage.mp3', true)
		xhr.responseType = 'arraybuffer';

		var sb = this.sb;

		xhr.onload = function(e) {
			sb.addSoundBuffer("testlyd", this.response);
		};

		xhr.send();

		

		//this.so = new this.vds.model.SoundObject("testsound", );
	});
})