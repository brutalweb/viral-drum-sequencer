require('waaclock');
Recorder = require('recorderjs');
var extend = require('util')._extend;

var ViralDrumSequencer = function(audioContext,booleanInitialize) {
		//for referencing this in later context;
		var VDSself = this;

		var idCounter = 0;

		//TODO make this test specific for AudioContext with all prefixes?
		if(typeof audioContext === "undefined") {
			audioContext = new AudioContext();
		};
		var scheduler = {
			bpm : 75
		}

		var webAudio = {
			ctx : audioContext,
			mainGain : audioContext.createGain(),
			recorderGain : audioContext.createGain(),
			connectSpeakersDirectly : function() {
				//this will output sound to the speakers without them being scheduled.
				//Because the scheduler will connect to the destination as well.
				//So really this is just for playing the sounds on user input
				webAudio.mainGain.connect(webAudio.ctx.destination);
			}
		};
		this.recorder = new Recorder(webAudio.recorderGain);

		//this object holds the sound banks that are currently active
		var activeSoundBanks = {};

		//putting soundBufferObjects here instead of holding buffers on the SoundObject.
		//Key is ID
		this.soundBuffers = {};

		var model = {
			SoundObject : function(soundName) {
				this.name = soundName;
				//must be loaded via the sound bank
				var soundBufferID = null;
				this.setId = function(id) {
					if(soundBufferID === null) {
						soundBufferID = id;
					}
				}
				//TODO support panning
				//this.panner = webAudio.ctx.createPanner();
				this.play = function(time) {
					var source = webAudio.ctx.createBufferSource();
					console.log(soundBufferID + soundName)
					source.buffer = vds.soundBuffers[soundBufferID];
					source.connect(webAudio.mainGain);
					source.connect(webAudio.recorderGain);
					if(time === undefined) {
						source.start(0);
					}

					else {
						console.log("playing sound " + name + "in " + time + " id " + soundBufferID)
						source.start(time);
					}
				}
				return this;
			},
			SoundBank : function(name) {
				var sb = this;
				this.name = name;
				this.sounds = {};
				this.addSoundBuffer = function(soundName, arraybuffer) {
					webAudio.ctx.decodeAudioData(arraybuffer, function(buffer) {
						var soundId = ++idCounter;
						sb.sounds[soundName].setId(soundId);
						VDSself.soundBuffers[soundId] = buffer;
					});
					
				}
				this.register = function(soundObject) {
					this.sounds[soundObject.name] = soundObject;
				}
				return this;
			},
			Pattern : function(resolution, beats, mapOfSoundBanks) {
				var PSelf = this;
				/*Every sound in the soundbanks in the mapOfSoundBanks
				 has resolution times beats number of slots in an array where 
				 "true" is set for it to play, else the slot is null or false
				 but that is when not running.
				 When running the "true" values will have an Event from the WAAClock
				 instead.
				 Can be built out later for MIDI signals for synths.

				 If the pattern is running, please do not change the pattern arrays
				 of the sounds in the sound banks directly, but call toggleSound.
				 Same with tempo changes - call the library public method changeBpm.
				 */
				 if(typeof mapOfSoundBanks === "undefined") {
				 	mapOfSoundBanks = VDSself.getSoundBanks();
				 }
				//copy the map of sound banks so that it is permanent - we are going to add
				//an array to each of the sound objects to represent the pattern.
				this.mapOfSoundBanks = extend({}, mapOfSoundBanks);
				if(typeof resolution ==="undefined") {
					this.resolution = 4;
				}
				else {
					this.resolution = resolution;
				}
				if(typeof length ==="undefined") {
					this.beats = 4;
				}
				else {
					this.beats = beats;
				}
				for(soundBank in this.mapOfSoundBanks) {
					for(sound in this.mapOfSoundBanks[soundBank].sounds) {
						//this array will hold the boolean value true for every
						//slot where it should play when not playing and
						//a WAAClock Event when playing
						this.mapOfSoundBanks[soundBank].sounds[sound].pattern = new Array(this.resolution*this.beats);
					}
				}
				this.running = false;
				this.runningAtSlot = null;
				this.runningUpdateTimer;
				//the clock is kept private.
				var clock = new WAAClock(webAudio.ctx);
				//Utility methods
				var events = [];
				//returns an array of events or null if not running
				function getAllEvents() {
					return events;					
				}
				function wipeEvents() {
					events = [];
				}
				function convertSoundPatternsToBoolean() {
					for(soundBank in PSelf.mapOfSoundBanks) {
						for(sound in PSelf.mapOfSoundBanks[soundBank].sounds) {
							soundObj = PSelf.mapOfSoundBanks[soundBank].sounds[sound];
							for(slot in soundObj.pattern) {
								if(typeof slot === "object") {
									slot = true;
								}
								else {
									slot = false;
								}
							}
						}
					}					
				}

				function getPatternDuration() {
					return (60/VDSself.getBpm())*PSelf.beats;
				}

				function convertSoundPatternBooleansToEvents() {

					for(soundBank in PSelf.mapOfSoundBanks) {
						for(sound in PSelf.mapOfSoundBanks[soundBank].sounds) {
							soundObj = PSelf.mapOfSoundBanks[soundBank].sounds[sound];
							for(i = 0; i <= soundObj.pattern.length-1; i++) {
								if(soundObj.pattern[i] == true) {
									//http://tobyho.com/2011/11/02/callbacks-in-loops/
									!function(soundObj, i) {
										events.push(clock.callbackAtTime(function(e) {
											soundObj.play(e.deadline);
										}, parseFloat(getDelayForSlot(i))+parseFloat(webAudio.ctx.currentTime)).repeat(getPatternDuration()));
									}(soundObj, i);
								}
							}
						}
					}					
				}
				//starts with 0
				function getDelayForSlot(slot) {
					var delaySecondsPerSlot = 60/VDSself.getBpm()/PSelf.resolution;
					return slot * delaySecondsPerSlot;
				}

				function updateRunningAtSlot(number) {
					PSelf.runningAtSlot = number;
					if(number < PSelf.beats*PSelf.resolution) {
						nextSlot = number + 1
					}
					else nextSlot = 1;
					PSelf.runningUpdateTimer = setTimeout(function() {
						updateRunningAtSlot(nextSlot);
					}, getDelayForSlot(1)*1000);
					var event = document.createEvent("Event");

					event.initEvent("slotChange",true,true);
					window.dispatchEvent(event);

				}

				function resetRunningAtSlot() {
					if(PSelf.runningUpdateTimer != null) {
						clearInterval(PSelf.runningUpdateTimer);
						PSelf.runningUpdateTimer = null;
					}
				}

				//PUBLIC API

				//start the beat
				this.start = function() {
					//schedule the events according to global bpm
					clock.start();
					convertSoundPatternBooleansToEvents();
					PSelf.running = true;
					updateRunningAtSlot(1);
				};
				//stop the beat
				this.stop = function() {
					var events = getAllEvents()
					for(eventIndex in events) {
						events[eventIndex].clear();
					}
					wipeEvents();
					PSelf.running = false;
					clock.stop();
					resetRunningAtSlot();
				};
				//turn on or off one of the slots and schedule or unschedule the event
				this.toggleSound = function(soundBankName, soundName, position) {
					var state = this.mapOfSoundBanks[soundBankName][soundName].pattern[position];
					if(typeof state === "undefined" || !state) {
						if(this.runningAtSlot != null) {
							//TODO if the pattern is playing, also change scheduling
						}
						state = true;
					}
					else {
						if(this.runningAtSlot != null) {
							//cancel the event
						}
						state = false;
					}
				};
				this.changeBpmDuringRun = function(newBpm) {
					//only if there are events running on the clock, else ignore.
					if(PSelf.runningAtSlot != null) {
						//TODO get all the event objects from the sound.patterns in the soundbanks.
						console.log("stretching schedules: " + VDSself.getBpm() / newBpm)
						clock.timeStretch(webAudio.ctx.currentTime, getAllEvents(), VDSself.getBpm() / newBpm);
					}
					VDSself.changeBpm(newBpm);
				};
				this.getAllSounds = function() {
					var returnVal = new Array();
					for(soundBank in mapOfSoundBanks) {
					for(sound in mapOfSoundBanks[soundBank]) {
						returnVal.add(sound);
					}
					return returnVal;
				}
				}
				//TODO methods for changing resolution and number of beats
				return this;
			}				
		};
		//PUBLIC API
		//make the model available public
		this.model = model;
		this.getBpm = function() {
			return scheduler.bpm;
		};
		this.changeBpm = function(newBpm) {
			//timeStretch every event in all running patterns.
			//remember to throw if something goes wrong.
			//set the bpm
			scheduler.bpm = newBpm;
		}		
		this.addSoundBank = function(soundBank) {
			activeSoundBanks[soundBank.name] = soundBank;
		};
		this.getSoundBanks = function() {
			return activeSoundBanks;
		};
		this.removeSoundBank = function(soundBankName) {
			delete activeSoundBanks[soundBankName];
		};
		this.initialize = function() {
			webAudio.connectSpeakersDirectly();
		};
		if(booleanInitialize) {
			this.initialize();
		}
	};
module.exports = ViralDrumSequencer;
if (typeof window !== 'undefined') window.ViralDrumSequencer = ViralDrumSequencer;