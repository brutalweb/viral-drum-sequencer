	var vds = new ViralDrumSequencer();
	vds.initialize();
	var drumboxModule = angular.module('viral-drum-sequencer', []);

	drumboxModule.controller('DrumboxController', ['$scope', '$http', function($scope, $http) {
		$scope.bpmSlide = vds.getBpm();
		var soundBank = new vds.model.SoundBank("drumbox")
		vds.addSoundBank(soundBank);

		var soundFileNames = ['cl_hihat', 'claves', 'conga1', 'cowbell', 'crashcym', 'handclap', 'hi_conga', 'hightom', 'kick1', 'kick2', 'maracas', 'open_hh', 'rimshot', 'snare', 'tom1'];
		soundFileNames.forEach(function(filename) {
			soundBank.register(new vds.model.SoundObject(filename));
		});
		$scope.pattern = new vds.model.Pattern(4,4);
		$scope.sounds = $scope.pattern.mapOfSoundBanks.drumbox.sounds;
		soundFileNames.forEach(function(filename) {
			var url = 'drumkit/' + filename + '.mp3';
			$http.get(url, {responseType : 'arraybuffer'}).success(function(data, status, headers, config) {
				soundBank.addSoundBuffer(filename, data);
			}).error(function(data, status, headers, config) {
				log("error status " + status);
			});
		});
		$scope.startBox = function() {
			$scope.pattern.start();
			document.getElementById("startBoxButton").disabled = true;

		};
		$scope.stopBox = function() {
			$scope.pattern.stop();
			document.getElementById("startBoxButton").disabled = false;
			var cells = document.querySelectorAll("#drumstable td");
			[].forEach.call(cells, function(cell) {
  				cell.classList.remove("activeCell");
			});
		};
		$scope.tempoChange = function() {
			$scope.pattern.changeBpmDuringRun($scope.bpmSlide);
		}

		$scope.$watch("pattern.runningAtSlot", function(oldVal, newVal) {
			console.log(oldVal, newVal)
			if(!isNaN(oldVal) && oldVal !== null) {
				var priorRow = document.querySelectorAll("#drumstable tr td:nth-child(" + (oldVal+1) + ")");
					[].forEach.call(priorRow, function(cell) {
  						cell.classList.add("activeCell");
					});
			}
			if(!isNaN(newVal) && newVal !== null) {
				var currentRow = document.querySelectorAll("#drumstable tr td:nth-child(" + (newVal+1) + ")");
					[].forEach.call(currentRow, function(cell) {
  						cell.classList.remove("activeCell");
					});
			}
		});

		window.addEventListener('slotChange', function (e) { if(!$scope.$$phase){$scope.$apply()} }, false);
	}]);